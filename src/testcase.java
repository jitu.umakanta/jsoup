import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by jitu on 3/30/2017.
 */
public class testcase {

    public static void main(String[] args) throws IOException {
        ArrayList al = new ArrayList();

        Document document = Jsoup.connect("http://www.reuters.com/").get();
        Elements links = document.select("body");

        for (Element e : links) {

            Elements pngs = e.select("img[src]");
            for (Element e1 : pngs) {
                System.out.println(e1);
            }

            Element content = e.getElementById("content");
            Elements links5 = content.getElementsByTag("img");
            for (Element link : links5) {
                String linkHref = link.attr("src");
               System.out.println(linkHref);
            }
        }

    }

    //if the element has child
    public static boolean hasChild(Element e) throws IOException {
        boolean b;
        ArrayList al = new ArrayList();

        Elements children = e.children();
        for (Element child : children) {
            al.add(child);
        }


        if (al.isEmpty() == true) {
            b = false;
            // System.out.println("empty");
        } else {
            b = true;
            // System.out.println("not empty");
        }

        //System.out.println(b);
        return b;


    }

    //total no of sibling elements
    public static int totalSiblings(Element e) {
        ArrayList al = new ArrayList();
        int i;

        Elements ell = e.siblingElements();
        for (Element e6 : ell) {
            al.add(e6);

        }
        i = al.size();
        if (i == 0) {
            i = 0;
        }
        return i;
    }

    //getting the attribute name in string
    public static String attributesName(Element e) {
        String convert = "no";
        Attributes at = e.attributes();
        convert = at.toString();
        return convert;
    }


}
